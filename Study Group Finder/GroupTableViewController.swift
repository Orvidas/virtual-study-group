//
//  CourseTableViewController.swift
//  Study Group Finder
//
//  Created by ubicomp6 on 9/28/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class GroupTableViewController: UITableViewController {
    
    @IBOutlet weak var joinGroupButton: UIBarButtonItem!
    var groupList: [Group] = []
    var selectedGroup: Group?
    var groupRef = FIRDatabase.database().reference().child("groups")
    var groupMemberRef = FIRDatabase.database().reference().child("groups-members").child((FIRAuth.auth()?.currentUser?.uid)!)
    var displayName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let displayNameQuery = FIRDatabase.database().reference().child("displayName").child((FIRAuth.auth()?.currentUser?.uid)!).queryOrderedByPriority()
        
        displayNameQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            self.displayName = snapshot.value as! String?
            
            self.title = self.displayName!
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getGroups()
    }
    
    private func getGroups() {
        
        let groupsIDQuery = groupMemberRef.queryOrderedByPriority()
        
        groupsIDQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            var groupsID: [String] = []
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                
                if (rest.value as? Bool) == true {
                    groupsID.append(rest.key)
                }
            }
            
            let groupNameQuery = self.groupRef.queryOrderedByPriority()
            
            groupNameQuery.observeSingleEvent(of: .value, with: { snapshot in
                
                var newGroupList:[Group] = []
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    
                    if groupsID.contains(rest.key), let dict = rest.value as? [String:Any], let name = dict["groupName"] as? String, let subName = dict["groupSubName"] as? String, let desc = dict["groupDesc"] as? String {
                        newGroupList.append(Group(name: name, subName: subName, desc: desc, id:rest.key))
                    }
                }
                
                self.groupList = newGroupList
                
                self.tableView.reloadData()
            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedGroup = groupList[indexPath.row]
        self.performSegue(withIdentifier: "ViewGroupChatSegue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath)
        
        cell.textLabel?.text = groupList[(indexPath as NSIndexPath).row].name
        cell.detailTextLabel?.text = groupList[(indexPath as NSIndexPath).row].subName
        
        return cell
    }
    
    @IBAction func joinGroupAction(_ sender: AnyObject) {
        sideMenuController?.performSegue(withIdentifier: "searchSegue", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewGroupChatSegue" {
            let chatVC = segue.destination as! GroupChatViewController
            chatVC.senderId = FIRAuth.auth()?.currentUser?.uid
            chatVC.senderDisplayName = self.displayName
            chatVC.group = selectedGroup
        }
    }
    
    @IBAction func unwindToGroupTable(segue: UIStoryboardSegue) {
        
    }
}
