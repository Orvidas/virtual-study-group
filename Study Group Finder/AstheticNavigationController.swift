//
//  AstheticNavigationController.swift
//  Study Group Finder
//
//  Created by ubicomp3 on 11/2/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit

class AstheticNavigationController: UINavigationController {
    //red: [UIColor colorWithRed:0.64 green:0.16 blue:0.16 alpha:1.0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barColor = UIColor(red: 0.85, green: 0, blue: 0, alpha: 1)        
        self.navigationBar.barTintColor = barColor
        
        self.navigationBar.barStyle = UIBarStyle.black
        self.navigationBar.tintColor = UIColor.white
        
        self.view.backgroundColor = UIColor(red:0.96, green:0.70, blue:0.42, alpha:1.0)
        
        //font size
        let titleAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 25)]
        self.navigationBar.titleTextAttributes = titleAttributes

        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
