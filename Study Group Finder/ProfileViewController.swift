//
//  UpdateProfilePictureViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 11/3/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var DisplayNameTextField: UITextField!
    @IBOutlet weak var ScrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var EmailTextLabel: UILabel!
    
    var hasImage = false
    
    var databaseRef = FIRDatabase.database().reference()
    var storageRef = FIRStorage.storage().reference()

    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        picker.delegate = self
        
        self.title = "Profile"
        
        self.EmailTextLabel.text = FIRAuth.auth()?.currentUser?.email!
        
        let displayNameQuery = FIRDatabase.database().reference().child("displayName").child((FIRAuth.auth()?.currentUser?.uid)!).queryOrderedByPriority()
        
        displayNameQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            self.DisplayNameTextField.text = snapshot.value as! String?
        })
        
        myImageView.contentMode = .scaleAspectFit
        DispatchQueue.global(qos: DispatchQoS.userInteractive.qosClass).async {
            self.databaseRef.child("avatarPicture").child(FIRAuth.auth()!.currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
                if let fileURL = snapshot.value as? String{
                
                    FIRStorage.storage().reference(forURL: fileURL).data(withMaxSize: 100*1024*1024, completion: { (data, error) in
                        if data != nil{
                            let userPhoto = UIImage(data: data!)
                            self.hasImage = true
                            DispatchQueue.main.async{
                                self.loadingIndicator.isHidden = true
                                self.myImageView.isHidden = false
                                self.myImageView.image = userPhoto
                            }
                        }
                    })
                }else{
                    let userPhoto = UIImage(named: "noimage.jpg")
                    DispatchQueue.main.async{
                        self.loadingIndicator.isHidden = true
                        self.myImageView.isHidden = false
                        self.myImageView.image = userPhoto
                    }
                }
            })
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToGroupTableSegue", sender: self)
    }
    
    @IBAction func chooseButtonPressed(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.allowsEditing = false
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        else {
            let alertVC = UIAlertController(title: "gallery mia", message: "gallery mia", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok", style:.default, handler: nil)
            alertVC.addAction(okAction)
            present(alertVC, animated: true, completion: nil)
        }
    }

    @IBAction func submitButtonPressed(_ sender: AnyObject) {
        
        chooseButton.isEnabled = false
        submitButton.isEnabled = false
        
        if myImageView.image != nil && hasImage{
            uploadToFirebase(chosenImage: myImageView.image!)
        }else{
            setDisplayName()
        }
    }
    
    func setDisplayName(){
        let itemRef = FIRDatabase.database().reference().child("displayName").child(FIRAuth.auth()!.currentUser!.uid)
        
        itemRef.setValue(self.DisplayNameTextField.text!, withCompletionBlock: { (error, ref) -> Void in
            if error == nil {
                let alertController = UIAlertController(title: "Profile Updated", message: "Profile updated.", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.chooseButton.isEnabled = true
                    self.submitButton.isEnabled = true
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func uploadToFirebase(chosenImage: UIImage) {
        let storageReference = storageRef.child("avatarPicture").child((FIRAuth.auth()?.currentUser?.uid)!)
        let imageData = UIImagePNGRepresentation(chosenImage)
        let imageReference = storageReference.child("avatarPicture.png")
        let metaData = FIRStorageMetadata()
        metaData.contentType = "image/png"
        imageReference.put(imageData!, metadata: metaData, completion: {(metaData,error) in
            if let error = error {
                let alertController = UIAlertController(title: "Oops!", message: error.localizedDescription, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.chooseButton.isEnabled = true
                    self.submitButton.isEnabled = true
                    
                })
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }else{
                let downloadURL = metaData!.downloadURL()!.absoluteString
                self.databaseRef.child("avatarPicture").child(FIRAuth.auth()!.currentUser!.uid).setValue(downloadURL, withCompletionBlock: { (error, ref) -> Void in
                    
                    if(error != nil){
                        let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                            self.chooseButton.isEnabled = true
                            self.submitButton.isEnabled = true
                            
                        })
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.setDisplayName()
                    }
                    
                })
            }
            
        })
    }
    private func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        myImageView.contentMode = .scaleAspectFit
        myImageView.image = chosenImage
        hasImage = true
        
        dismiss(animated:true, completion: nil)
    }
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = keyboardFrame.height
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = 0
        
    }
}
