//
//  SearchGroupViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 11/16/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class SearchGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    
    var groupRef = FIRDatabase.database().reference().child("groups")
    var groupMemberRef = FIRDatabase.database().reference().child("groups-members")
    
    var selectedId = ""
    
    var groups = [Group]()
    var filtered = [Group]()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //font
        let appearance = UITabBarItem.appearance()
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        
        tableViewBottom.constant = (self.tabBarController?.tabBar.frame.height)!
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.enablesReturnKeyAutomatically = false
        
        let addButton = UIBarButtonItem(title: "Create Group", style: .plain, target: self, action: #selector(createGroup))
        self.tabBarController?.navigationItem.rightBarButtonItem  = addButton
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        getGroups()
    }
    
    func createGroup(){
        self.performSegue(withIdentifier: "CreateGroupSegue", sender: self)
    }
    
    private func getGroups() {
        
        let groupsQuery = groupRef.queryOrderedByPriority()
        
        groupsQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            var newGroupList:[Group] = []
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                
                if let dict = rest.value as? [String : Any], let name = dict["groupName"] as? String {
                    newGroupList.append(Group(name: name, subName: "", desc: "", id:rest.key))
                }
            }
            
            self.groups = newGroupList
            
            self.tableView.reloadData()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if (searchBar.text?.characters.count)! > 0 {
            searchActive = true;
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = groups.filter({ (group) -> Bool in
            let tmp = NSString(string: group.name)
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell", for: indexPath)
        
        let index = indexPath.row
        
        if(searchActive){
            cell.textLabel?.text = filtered[index].name
        } else {
            cell.textLabel?.text = groups[index].name
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(searchActive){
            selectedId = filtered[indexPath.row].id
        } else {
            selectedId = groups[indexPath.row].id
        }
        
        self.performSegue(withIdentifier: "ShowGroupProfileSegue", sender: self)
        
//        let index = indexPath.row
//        var groupName = ""
//        var groupId = ""
//        if(searchActive){
//            groupName = filtered[index].name
//            groupId = filtered[index].id
//        } else {
//            groupName = groups[index].name
//            groupId = groups[index].id
//        }
//        
//        let alertController = UIAlertController(title: "Join Group?", message: "Do you want to join group \(groupName)?", preferredStyle: .alert)
//        
//        let acceptAction = UIAlertAction(title: "Yes", style: .default, handler: {(alert: UIAlertAction!) in
//            let itemRef = self.groupMemberRef.child((FIRAuth.auth()?.currentUser?.uid)!).child(groupId)
//            itemRef.setValue(true)
//            
//        })
//        
//        let declineAction = UIAlertAction(title: "No", style: .default, handler: nil)
//        
//        alertController.addAction(acceptAction)
//        alertController.addAction(declineAction)
//        
//        self.present(alertController, animated: true, completion: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowGroupProfileSegue" {
            
            if let gpVC = segue.destination as? GroupProfileViewController {
                gpVC.groupId = selectedId
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.tableViewBottom.constant = keyboardFrame.height
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.tableViewBottom.constant = (self.tabBarController?.tabBar.frame.height)!
        
    }

}
