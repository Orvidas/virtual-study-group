//
//  Group.swift
//  Study Group Finder
//
//  Created by Ka Lum on 10/29/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit

class Group: NSObject {
    let name: String
    let subName: String
    let desc : String
    let id: String
    
    init(name: String, subName: String, desc: String, id: String){
        self.name = name
        self.subName = subName
        self.desc = desc
        self.id = id
    }
}
