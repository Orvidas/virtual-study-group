//
//  MenuTableViewController.swift
//  
//
//  Created by ubicomp2 on 11/1/16.
//
//

import UIKit
import Firebase

class MenuTableViewController: UITableViewController {

    let cells = ["Profile", "My Groups", "Search", "Logout"]
    
    private var previousIndex: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44.0;
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = cells[indexPath.row]
        return cell
    }
    
    //menu selections
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {

        tableView.deselectRow(at: indexPath as IndexPath, animated: true)

        let currentCell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        
        if currentCell.textLabel?.text == "Logout"{
            try! FIRAuth.auth()!.signOut()
            sideMenuController?.performSegue(withIdentifier: "logoutSegue", sender: nil)
        }else if currentCell.textLabel?.text == "Profile"{
            sideMenuController?.performSegue(withIdentifier: "profileSegue", sender: nil)
        }else if currentCell.textLabel?.text == "My Groups"{
            sideMenuController?.performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
        }else if currentCell.textLabel?.text == "Direct Messages"{
            sideMenuController?.performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
        }else if currentCell.textLabel?.text == "Search"{
            sideMenuController?.performSegue(withIdentifier: "searchSegue", sender: nil)
        }
    }

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
