//
//  GroupProfileViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 12/6/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class GroupProfileViewController: UIViewController {

    var groupId : String?
    
    @IBOutlet weak var GroupNameLabel: UILabel!
    @IBOutlet weak var GroupSubNameLabel: UILabel!
    @IBOutlet weak var GroupDescTextView: UITextView!
    @IBOutlet weak var GroupButton: UIButton!
    
    var inGroup = false
    
    var groupRef = FIRDatabase.database().reference().child("groups")
    var groupMemberRef = FIRDatabase.database().reference().child("groups-members").child((FIRAuth.auth()?.currentUser?.uid)!)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if groupId != nil{
            getGroupInfo()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func getGroupInfo() {
        
        let groupQuery = groupRef.child(groupId!).queryOrderedByPriority()
        
        groupQuery.observeSingleEvent(of: .value, with: { snapshot in
   
            if let dict = snapshot.value as? [String:Any], let name = dict["groupName"] as? String, let subName = dict["groupSubName"] as? String, let desc = dict["groupDesc"] as? String {
                self.GroupNameLabel.text = name
                self.GroupSubNameLabel.text = subName
                self.GroupDescTextView.text = desc
            }
        })
        
        let groupMemberQuery = groupMemberRef.child(groupId!).queryOrderedByPriority()
        
        groupMemberQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            self.inGroup = snapshot.exists()
            
            print(self.inGroup)
            
            if(self.inGroup){
                self.GroupButton.setTitle("Leave Group", for: .normal)
            }else{
                self.GroupButton.setTitle("Join Group", for: .normal)
            }
            
            self.GroupButton.isEnabled = true
        })
    }
    @IBAction func GroupButtonPressed(_ sender: AnyObject) {
        if(inGroup){
            leaveGroup()
        }else{
            joinGroup()
        }
    }
    
    func joinGroup(){
        
        let itemRef = groupMemberRef.child(groupId!)
        itemRef.setValue(true, withCompletionBlock: { (error, ref) -> Void in
            print(error)
            if error == nil {
                let alertController = UIAlertController(title: "Nice!", message: "Group joined.", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.sideMenuController?.performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        })
        
        GroupButton.isEnabled = false
    }
    
    func leaveGroup(){
        groupMemberRef.child(groupId!).removeValue(completionBlock: { (error, ref) -> Void in
            print(error)
            if error == nil {
                let alertController = UIAlertController(title: "Nice!", message: "Group left.", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.sideMenuController?.performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
                    
                })
                
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        })
        
        GroupButton.isEnabled = false
    }
}
