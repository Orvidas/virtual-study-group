//
//  SearchStudentViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 11/16/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class SearchStudentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!

    var students = [Student]()
    var filtered = [Student]()
    var searchActive : Bool = false
    
    var selectedId = ""
    
    var studentRef = FIRDatabase.database().reference().child("displayName")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewBottom.constant = (self.tabBarController?.tabBar.frame.height)!
        
        //font
        let appearance = UITabBarItem.appearance()
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.enablesReturnKeyAutomatically = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        getStudents()
    }
    
    private func getStudents() {
        
        let studentQuery = studentRef.queryOrderedByPriority()
        
        studentQuery.observeSingleEvent(of: .value, with: { snapshot in
            
            var newStudentList:[Student] = []
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                
                if let displayName = rest.value as? String {
                    newStudentList.append(Student(displayName: displayName, id:rest.key))
                }
            }
            
            self.students = newStudentList
            
            self.tableView.reloadData()
        })
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if (searchBar.text?.characters.count)! > 0 {
            searchActive = true;
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = students.filter({ (student) -> Bool in
            let tmp = NSString(string: student.displayName)
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath)
        
        let index = indexPath.row
        
        if(searchActive){
            cell.textLabel?.text = filtered[index].displayName
        } else {
            cell.textLabel?.text = students[index].displayName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(searchActive){
            selectedId = filtered[indexPath.row].id
        } else {
            selectedId = students[indexPath.row].id
        }
        
        //self.performSegue(withIdentifier: "ShowGroupProfileSegue", sender: self)
        
    }

    
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.tableViewBottom.constant = keyboardFrame.height
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.tableViewBottom.constant = (self.tabBarController?.tabBar.frame.height)!
        
    }
}
