//
//  FilesViewController.swift
//  Study Group Finder
//
//  Created by Orlando Balderas on 11/4/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class FilesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var filesTableView: UITableView!
    var imageNames: [String] = []
    var images: [UIImage] = []
    var group: Group?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createFilesNavButton()
        getImagesFromFirebase()
        self.filesTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        filesTableView.reloadData()
    }
    
    func getImagesFromFirebase() {
        guard let currentGroup = group else {
            presentErroAlert(title: "No images found", message: "There were no images found for this group")
            return
        }
        let databaseReference = FIRDatabase.database().reference().child("groups").child(currentGroup.id).child("imageUploads")
        databaseReference.observe(.childAdded, with: { (snapshot) in
            guard let downloadURL = snapshot.value as? String, let key = snapshot.key as String? else {
                self.presentErroAlert(title: "No images in this group", message: "No one in this group has uploaded any images")
                return
            }
            self.imageNames.append(key)
            let storageReference = FIRStorage.storage().reference(forURL: downloadURL)
            storageReference.data(withMaxSize: 100 * 1024 * 1024, completion: { (data, error) in
                guard error == nil else {
                    self.presentErroAlert(title: "Oops", message: (error?.localizedDescription)!)
                    return
                }
                guard data != nil else {
                    self.presentErroAlert(title: "Data cannot be found", message: "Error message not found")
                    return
                }
                let image = UIImage(data: data!)
                self.images.append(image!)
                //print("The image count during is " + String(self.images.count))
                self.filesTableView.reloadData()
            })
        })
    }
    
    func presentErroAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filesTableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        if images.count != 0 && imageNames.count != 0 {
            cell.textLabel?.text = imageNames[indexPath.row]
            cell.imageView?.image = images[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIImageWriteToSavedPhotosAlbum(images[indexPath.row], nil, nil, nil)
        
        let alertController = UIAlertController(title: "Image saved", message: "The selected image has been saved to your photo library", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func createFilesNavButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Upload", style: .plain, target: self, action: #selector(self.segueToFiles))
    }
    
    func segueToFiles() {
        performSegue(withIdentifier: "UploadSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! UploadFileViewController
        
        destination.group = group
    }
    
    @IBAction func unwindFromUploadFiles(segue: UIStoryboardSegue) {
        
    }
}
