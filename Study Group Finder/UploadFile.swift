//
//  UploadFile.swift
//  Study Group Finder
//
//  Created by Orlando Balderas on 10/25/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import Foundation
import Firebase

class UploadFile {
    func uploadFileFromDevice(fileName: String, filePath: String) {
        let file = NSURL(fileURLWithPath: filePath)

        if fileName.characters.count > 0 {
            let storageReference = FIRStorage.storage().reference().child("fileUploads/" + fileName)
            storageReference.putFile(file as URL)
        }
    }
}
