//
//  CreateGroupViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 12/6/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class CreateGroupViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var GroupNameTextField: UITextField!
    @IBOutlet weak var GroupSubNameTextField: UITextField!
    @IBOutlet weak var GroupDescTextView: UITextView!
    @IBOutlet weak var CreateGroupButton: UIButton!
    @IBOutlet weak var ScrollViewBottom: NSLayoutConstraint!
    
    var groupRef = FIRDatabase.database().reference().child("groups")
    var groupMemberRef = FIRDatabase.database().reference().child("groups-members")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GroupNameTextField.delegate = self
        GroupSubNameTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func CreateGroupButtonPressed(_ sender: AnyObject) {
        CreateGroupButton.isEnabled = false
        if(GroupNameTextField.text != ""){
            
            groupRef.observeSingleEvent(of: .value, with: { snapshot in
                
                var GroupExists:Bool = false
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    
                    if let a = rest.value as? [String:Any], let groupName = a["groupName"] as? String, groupName == self.GroupNameTextField.text!{
                        GroupExists = true
                    }
                    
                }
                if GroupExists{
                    let alertController = UIAlertController(title: "Oops!", message: "Group already exists.", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                        self.CreateGroupButton.isEnabled = true
                        
                    })
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    
                    let itemRef = self.groupRef.childByAutoId()
                    let groupItem = ["groupName": self.GroupNameTextField.text!, "groupSubName": self.GroupSubNameTextField.text!, "groupDesc": self.GroupDescTextView.text!]
                    itemRef.setValue(groupItem)
                    
                    let roomid = itemRef.key
                    
                    let itemRef2 = self.groupMemberRef.child((FIRAuth.auth()?.currentUser?.uid)!).child(roomid)
                    itemRef2.setValue(true)
                    
                    let alertController = UIAlertController(title: "Nice!", message: "Group created.", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                        self.sideMenuController?.performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
                        
                    })
                    
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            })
            
        }else{
            let alertController = UIAlertController(title: "Oops!", message: "Group Name Field is empty.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case GroupNameTextField:
            GroupSubNameTextField.becomeFirstResponder()
            break
        case GroupSubNameTextField:
            GroupDescTextView.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }

    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = keyboardFrame.height
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = 0
        
    }
}
