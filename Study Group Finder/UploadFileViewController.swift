//
//  UploadFileViewController.swift
//  Study Group Finder
//
//  Created by ubicomp5 on 10/31/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class UploadFileViewController: UIViewController, UIImagePickerControllerDelegate,  UINavigationControllerDelegate{
    let picker = UIImagePickerController()
    var group: Group?
    var isImageSelected = false
    var selectedImage: UIImage?
    @IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var mySubmitButton: UIButton!
    @IBOutlet weak var ScrollViewBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        mySubmitButton.isEnabled = false
        picker.delegate = self
        myTextField.addTarget(self, action: #selector(self.checkTextField(sender:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        myImageView.contentMode = .scaleAspectFit
        myImageView.image = chosenImage
        selectedImage = chosenImage
        isImageSelected = true
        if !(myTextField.text?.isEmpty)! {
            mySubmitButton.isEnabled = true
        }
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func checkTextField(sender: UITextField) {
        if !(sender.text?.isEmpty)! && isImageSelected {
            mySubmitButton.isEnabled = true
        }
        else {
            mySubmitButton.isEnabled = false
        }
    }
    
    @IBAction func selectImage(_ sender: AnyObject) {
        picker.allowsEditing = false;
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        uploadImageToFirebase(chosenImage: selectedImage!)
        unwindToFilesView()
    }
    
    func uploadImageToFirebase(chosenImage: UIImage) {
        guard let title = myTextField.text else{
            return
        }
        let storageReference = FIRStorage.storage().reference().child((group?.id)!).child("imageUploads")
        var databaseReference: FIRDatabaseReference!
        let imageData = UIImagePNGRepresentation(chosenImage)
        let imageReference = storageReference.child(title)
        let metaData = FIRStorageMetadata()
        metaData.contentType = "image/png"
        
        imageReference.put(imageData!, metadata: metaData, completion: {
            (metaData, error) in
            guard error == nil else {
                self.presentErroAlert(error: error!)
                return
            }
            let downloadURL = metaData?.downloadURL()?.absoluteString
            databaseReference = FIRDatabase.database().reference().child("groups").child((self.group?.id)!).child("imageUploads").child(title)
            databaseReference.setValue(downloadURL, withCompletionBlock: {(newError, ref) in
                guard newError == nil else {
                    self.presentErroAlert(error: newError!)
                    return
                }
                let alertController = UIAlertController(title: "Image uploaded", message: "Entire group can now view this image", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
            })
        })
    }
    
    func presentErroAlert(error: Error) {
        let alertController = UIAlertController(title: "Oops!", message: error.localizedDescription, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func createFilesNavButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(self.unwindToFilesView))
    }
    
    func unwindToFilesView() {
        self.performSegue(withIdentifier: "UnwindToFilesViewSegue", sender: self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = keyboardFrame.height
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.ScrollViewBottom.constant = 0
        
    }
}
