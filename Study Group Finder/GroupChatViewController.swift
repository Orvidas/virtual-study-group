//
//  CourseChatViewController.swift
//  Study Group Finder
//
//  Created by Orlando Balderas on 10/19/16.
//  Copyright © 2016 Team6. All rights reserved.
//
import UIKit
import Firebase
import JSQMessagesViewController

class GroupChatViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var rootRef = FIRDatabase.database().reference()
    var messageRef = FIRDatabase.database().reference().child("messages")
    var messages = [JSQMessage]()
    var messageIds = Set<String>()
    var avatars = [String: JSQMessagesAvatarImage]()
    var downloadedAvatars: Set<String> = []
    var databaseRef = FIRDatabase.database().reference()
    var storageRef = FIRStorage.storage().reference()
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    var group:Group?
    var testimage :JSQMessagesAvatarImage?
    
    var userIsTypingRef = FIRDatabase.database().reference()
    private var localTyping = false
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    
    var usersTypingQuery: FIRDatabaseQuery!
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createFilesNavButton()
        createAvatar(senderId: self.senderId)
        setupBubbles()
        self.title = group?.name
        messageRef = messageRef.child((group?.id)!)
        picker.delegate = self
        observeMessages()
        observeTyping()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        }
        else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let message = messages[indexPath.row]
        
        return avatars[message.senderId];
    }
    
    func createAvatar(senderId: String)
    {
        DispatchQueue.global(qos: DispatchQoS.userInteractive.qosClass).async {
            if !self.downloadedAvatars.contains(senderId){
                self.downloadedAvatars.insert(senderId)
                self.databaseRef.child("avatarPicture").child(senderId).observeSingleEvent(of: .value, with: { (snapshot) in
            
                    if let fileURL = snapshot.value as? String{
                
                        FIRStorage.storage().reference(forURL: fileURL).data(withMaxSize: 100*1024*1024, completion: { (data, error) in
                                if data != nil{
                                    let userPhoto = UIImage(data: data!)
                                    self.avatars[senderId] = JSQMessagesAvatarImageFactory.avatarImage(with: userPhoto, diameter: 30)
                                    
                                    DispatchQueue.main.async{
                                        self.collectionView.reloadData()
                                    }
                                }
                        })
                    }
                })
            }
        }
    }
    
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleBlue())
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(
            with: UIColor.jsq_messageBubbleLightGray())
    }
    
    func addTextMessage(id: String, text: String, displayName: String) {
        let message = JSQMessage(senderId: id, displayName: displayName, text: text)
        
        messages.append(message!)
    }
    
    func addMediaMessage(id: String, image: UIImage, displayName: String){
        let mediaItem = JSQPhotoMediaItem(image: image)
        mediaItem?.appliesMediaViewMaskAsOutgoing = true
        
        let sendMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, media: mediaItem)
        
        messages.append(sendMessage!)
    }
    
    func addEmptyMediaMessage(id: String, displayName: String) -> Int{
        let mediaItem = JSQPhotoMediaItem()
        if id == senderId {
            mediaItem.appliesMediaViewMaskAsOutgoing = true
        }else{
            mediaItem.appliesMediaViewMaskAsOutgoing = false
        }
        
        if let sendMessage = JSQMessage(senderId: id, displayName: displayName, media: mediaItem){
            
            messages.append(sendMessage)
            
            return messages.index(of: sendMessage)!
        }
        
        return -1
    }
    
    func downloadMediaContent(downloadURL: String, index: Int){
            FIRStorage.storage().reference(forURL: downloadURL).data(withMaxSize: 100*1024*1024, completion: { (data, error) in
                if data != nil{
                    let image = UIImage(data: data!)
                    DispatchQueue.main.async{
                        if let photoMediaItem = self.messages[index].media as? JSQPhotoMediaItem {
                            photoMediaItem.image = image
                            self.collectionView.reloadData()
                        }
                    }
                }
            })
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        if(!message.isMediaMessage){
            if message.senderId == senderId {
                cell.textView!.textColor = UIColor.white
            } else {
                cell.textView!.textColor = UIColor.black
            }
        }
        
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let itemRef = messageRef.childByAutoId()
        messageIds.insert(itemRef.key)
        
        let messageItem = ["text": text!,"senderId": senderId!, "senderDisplayName": senderDisplayName!, "isMedia": "false"]
        itemRef.setValue(messageItem)
        
        self.addTextMessage(id: senderId, text: text, displayName: senderDisplayName)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        
        isTyping = false
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.allowsEditing = false
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        }
        else {
            let alertVC = UIAlertController(title: "gallery mia", message: "gallery mia", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok", style:.default, handler: nil)
            alertVC.addAction(okAction)
            present(alertVC, animated: true, completion: nil)
        }
    }
    
    private func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            let mediaItem = JSQPhotoMediaItem(image: chosenImage)
            mediaItem?.appliesMediaViewMaskAsOutgoing = true
            
            if let sendMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, media: mediaItem){
                
                self.messages.append(sendMessage)
                self.collectionView.reloadData()
                
                let itemRef = messageRef.childByAutoId()
                messageIds.insert(itemRef.key)
                
                uploadToFirebase(chosenImage: chosenImage, messageId: itemRef.key)
                
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
            }
        }
       
        dismiss(animated:true, completion: nil)
    }
    
    func uploadToFirebase(chosenImage: UIImage, messageId: String) {
        let storageReference = storageRef.child("messagePicture").child((group?.id)!)
        let imageData = UIImagePNGRepresentation(chosenImage)
        let imageReference = storageReference.child("\(messageId).png")
        let metaData = FIRStorageMetadata()
        metaData.contentType = "image/png"
        imageReference.put(imageData!, metadata: metaData, completion: {(metaData,error) in
            if let error = error {
                let alertController = UIAlertController(title: "Oops!", message: error.localizedDescription, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    
                })
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }else{
                let downloadURL = metaData!.downloadURL()!.absoluteString
                let messageRef = self.messageRef.child(messageId)
                let messageItem = ["text": downloadURL, "senderId": self.senderId!, "senderDisplayName": self.senderDisplayName!, "isMedia": "true"]
                messageRef.setValue(messageItem)
                self.finishSendingMessage()
                
            }
            
        })
    }
    
    private func observeMessages() {
        
        let messagesQuery = messageRef.queryLimited(toLast: 25)
        
        messagesQuery.observe(.childAdded, with: { snap in
            if(!self.messageIds.contains(snap.key)){
                if let dict = snap.value as? NSDictionary, let id = dict["senderId"] as? String, let text = dict["text"] as? String, let displayName = dict["senderDisplayName"] as? String, let isMedia = dict["isMedia"] as? String
                {
                    if(isMedia == "false"){
                        self.addTextMessage(id: id, text: text, displayName: displayName)
                        self.createAvatar(senderId: id)
                        self.finishReceivingMessage()
                        self.messageIds.insert(snap.key)
                    }else{
                        
                        let index = self.addEmptyMediaMessage(id: id, displayName: displayName)
                        DispatchQueue.global(qos: DispatchQoS.userInteractive.qosClass).async {
                            self.downloadMediaContent(downloadURL: text, index: index)
                        }
                    }
                }
            }
        })
    }
    
    private func observeTyping() {
        let typingIndicatorRef = rootRef.child("typingIndicator").child((group?.id)!)
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        
        usersTypingQuery = typingIndicatorRef.queryOrderedByValue().queryEqual(toValue: true)
        
        usersTypingQuery.observe(.value, with: { snap in
            
            if snap.childrenCount == 1 && self.isTyping {
                return
            }
            
            self.showTypingIndicator = snap.childrenCount > 0
            self.scrollToBottom(animated: true)
            
        })
        
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        isTyping = textView.text != ""
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        userIsTypingRef.removeValue()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item];
        
        // Sent by me, skip
        if message.senderId == senderId {
            return nil;
        }
        
        // Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item - 1];
            if previousMessage.senderId == message.senderId {
                return nil;
            }
        }
        
        return NSAttributedString(string:message.senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let message = messages[indexPath.item]
        
        // Sent by me, skip
        if message.senderId == senderId {
            return CGFloat(0.0);
        }
        
        // Same as previous sender, skip
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item - 1];
            if previousMessage.senderId == message.senderId {
                return CGFloat(0.0);
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    
    func createFilesNavButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(self.segueToFiles))
    }
    func segueToFiles() {
        performSegue(withIdentifier: "FilesSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! FilesViewController
        
        destination.group = self.group
    }
}
