//
//  RegisterViewController.swift
//  Study Group Finder
//
//  Created by Ka Lum on 10/30/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var centerY: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        displayNameTextField.delegate = self
        passwordTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func submitButtonPressed(_ sender: AnyObject) {
        if self.emailTextField.text! == "" || self.passwordTextField.text! == "" || self.displayNameTextField.text! == ""
        {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter an email, display name and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            FIRAuth.auth()?.createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                if error == nil
                {
                    let itemRef = FIRDatabase.database().reference().child("displayName").child((user?.uid)!)
                    
                    itemRef.setValue(self.displayNameTextField.text!, withCompletionBlock: { (error, ref) -> Void in
                        print(error)
                        if error == nil {
                            self.performSegue(withIdentifier: "RegisterLoginSegue", sender: self)
                        }
                    })
                }
                else
                {
                    let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case emailTextField:
            displayNameTextField.becomeFirstResponder()
            break
        case displayNameTextField:
            passwordTextField.becomeFirstResponder()
            break
        case passwordTextField:
            submitButtonPressed(submitButton)
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.centerY.constant = -keyboardFrame.height / 2
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        self.centerY.constant = 0
        
    }
    
}
