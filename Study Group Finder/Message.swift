//
//  Message.swift
//  Study Group Finder
//
//  Created by Ka Lum on 10/30/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class Message : NSObject, JSQMessageData {
    
    var senderId_ : String!
    var senderDisplayName_ : String!
    var date_ : NSDate
    var isMediaMessage_ : Bool
    var messageHash_ : Int = 0
    var text_ : String
    
    init(senderId: String, senderDisplayName: String, isMediaMessage: Bool, messageHash: Int, text: String) {
        self.senderId_ = senderId
        self.senderDisplayName_ = senderDisplayName
        self.date_ = NSDate()
        self.isMediaMessage_ = isMediaMessage
        self.messageHash_ = messageHash
        self.text_ = text
    }
    
    func senderId() -> String! {
        return senderId_;
    }
    
    func senderDisplayName() -> String! {
        return senderDisplayName_;
    }
    
    func date() -> Date! {
        return date_ as Date!;
    }
    
    func isMediaMessage() -> Bool {
        return isMediaMessage_;
    }
    
    public func messageHash() -> UInt {
        return UInt(messageHash_);
    }
    
    func text() -> String! {
        return text_;
    }
}
