//
//  AppDelegate.swift
//  Study Group Finder
//
//  Created by ubicomp6 on 9/19/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase
import SideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        
        //side menu settings
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "Menu Filled-50-2")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = 300
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        
        //titleTextAttributes = [NSFontAttributeName : (UIFont(name: "FONT NAME", size: 18))!, NSForegroundColorAttributeName: UIColor.whiteColor] }
    
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.downloadTask(with: urlRequest) { (location, response, error) in
            let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            
            if let fileData = NSData(contentsOf: location!), let writePath = docPath?.appendingPathComponent(url.lastPathComponent) {
                fileData.write(to: writePath, atomically: false)
                
                let alertVC = UIAlertController(title: "Download successfull", message: "The file was successfully downloaded from the internet.", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alertVC.addAction(okAction)
                if let navVC = self.window?.rootViewController as? UINavigationController {
                    navVC.visibleViewController?.present(alertVC, animated: true, completion: nil)
                }
            }
            else {
                let alertVC = UIAlertController(title: "Download failed", message: "The file could not be downloaded to your documents folder.", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alertVC.addAction(okAction)
                if let navVC = self.window?.rootViewController as? UINavigationController {
                    navVC.visibleViewController?.present(alertVC, animated: true, completion: nil)
                }
            }
        }
        task.resume()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

