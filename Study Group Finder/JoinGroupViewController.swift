//
//  AddCourseViewController.swift
//  Study Group Finder
//
//  Created by ubicomp6 on 9/28/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit
import Firebase

class JoinGroupViewController: UIViewController {
    
    @IBOutlet weak var GroupNameTextField: UITextField!
    @IBOutlet weak var CreateGroupButton: UIButton!
    @IBOutlet weak var JoinGroupButton: UIButton!
    
    var groupRef = FIRDatabase.database().reference().child("groups")
    var groupMemberRef = FIRDatabase.database().reference().child("groups-members")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func CreateGroupButtonClicked(_ sender: AnyObject) {
        if(GroupNameTextField.text != ""){
            
            groupRef.observeSingleEvent(of: .value, with: { snapshot in
                
                var GroupExists:Bool = false
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    
                    let a = rest.value as! [String:String]
                    
                    if a["groupName"]! == self.GroupNameTextField.text!{
                        GroupExists = true
                    }
                    
                }
                if GroupExists{
                    let alertController = UIAlertController(title: "Oops!", message: "Group already exists.", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    
                    let itemRef = self.groupRef.childByAutoId()
                    let groupItem = ["groupName": self.GroupNameTextField.text!]
                    itemRef.setValue(groupItem)
                    
                    let roomid = itemRef.key
                    
                    let itemRef2 = self.groupMemberRef.child((FIRAuth.auth()?.currentUser?.uid)!).child(roomid)
                    itemRef2.setValue(true)
                    
                    
                    self.performSegue(withIdentifier: "unwindToGroupTableSegue", sender: self)
                }
            })
            
        }else{
            
        }
    }
    @IBAction func JoinGroupButtonClicked(_ sender: AnyObject) {
        if(GroupNameTextField.text != ""){
            
            groupRef.observeSingleEvent(of: .value, with: { snapshot in
                
                var roomid:String = ""
                
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    
                    let a = rest.value as! [String:String]
                    
                    if a["groupName"] == self.GroupNameTextField.text{
                        roomid = rest.key
                        break
                    }
                }
                
                if roomid != "" {
                    let itemRef = self.groupMemberRef.child((FIRAuth.auth()?.currentUser?.uid)!).child(roomid)
                    itemRef.setValue(true)
                    self.performSegue(withIdentifier: "unwindToGroupTableSegue", sender: self)
                    
                }else{
                    let alertController = UIAlertController(title: "Oops!", message: "Group doesn't exist.", preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
            
        }else{
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //let destination = segue.destination as! CourseTableViewController
        //destination.courseList.append(newCourse!)
    }
}
