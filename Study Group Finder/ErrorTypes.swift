//
//  ErrorTypes.swift
//  Study Group Finder
//
//  Created by ubicomp6 on 9/19/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import Foundation

enum CourseSelectionErrors: Error {
    case invalidCourseSelection    
}
