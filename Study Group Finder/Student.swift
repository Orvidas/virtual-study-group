//
//  Student.swift
//  Study Group Finder
//
//  Created by Ka Lum on 11/17/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import UIKit

class Student: NSObject {
    let displayName: String
    let id: String
    
    init(displayName: String, id: String){
        self.displayName = displayName
        self.id = id
    }
}
