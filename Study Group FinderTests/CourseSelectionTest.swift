//
//  CourseSelectionTest.swift
//  Study Group Finder
//
//  Created by ubicomp6 on 9/19/16.
//  Copyright © 2016 Team6. All rights reserved.
//

import Foundation
import XCTest
import Firebase
@testable import Study_Group_Finder

class CourseSelectionTest: XCTestCase {
    func testPickValidCourse() {
        do {
            try CourseSelection().findCourse("COSC", courseCode: "4355")
        }
        catch {
            assertionFailure("Invalid Course")
        }
    }
    
    func testPickInvalidCoursePrefix() {
        do {
            try CourseSelection().findCourse("COSCd", courseCode: "4355")
            assertionFailure("Valid Course in Invalid Test")
        }
        catch {
            assert(true)
        }
    }
    
    func testPickInvalidCourseCode() {
        do {
            try CourseSelection().findCourse("COSC", courseCode: "12345")
            assertionFailure("Valid Course in Invalid Test")
        }
        catch {
            assert(true)
        }
    }
}
